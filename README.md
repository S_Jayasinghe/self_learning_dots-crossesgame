# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Self Learning Dots & Crosses Game
* 1.0
* Sandunil Jayasinghe

### How do I get set up? ###

* JDBC Driver for Java-MySQL connection
* SQL files available
* Dependencies - mysql-connector-java-5.1.40-bin.jar
* Database configuration - As per the local system configuration
* How to run tests - starter.java is the Main Class which should be executed for results
* Deployment instructions - Locally Deployable with database connections

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin