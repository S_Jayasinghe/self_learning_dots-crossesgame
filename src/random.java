
import static java.awt.SystemColor.text;
import java.util.Random;
import java.sql.*;
import java.util.HashMap;

/**
 * random.class allows the program to select numbers from the number combinations from permutations of 9, 
 * represented in lexicographical order.
 *
 * provides methods to choose best next move depending on what the opponent and the current player has chosen 
 * so far as their movements in the game.
 *
 * 
 *
 *
 */

/**
 *
 * @author S.Jayasinghe
 */
public class random {
    public int getRandomWithExclusion(Random rnd, int start, int end, int... exclude) {
            int random = start + rnd.nextInt(end - start + 1 - exclude.length);
            for (int ex : exclude) {
                if (random < ex) {
                    break;
                }
                random++;
            }
            return random;
        }
    
    public String returnPattern(String current){
        int indexOfit;
        String tempSeq;
        int messageSize;
        String resultPlayer;
        Connection myConn=null;
        Statement stmt=null;
        ResultSet result=null;
        String user="root";
        String pass="";
        int size=0;
        int n=0;
        Random rand = new Random();
        HashMap <Integer,String> data= new HashMap <>();
        Random rn = new Random();
        String returnThis = "NOPE";
         try{
             
                n=0;
                size=0;
                data.clear();
                messageSize=0;
                tempSeq=null;
                
				myConn=DriverManager.getConnection("jdbc:mysql://localhost/learndots",user,pass);
				stmt=myConn.createStatement();
				result=stmt.executeQuery("SELECT * FROM `lexi` WHERE move like '"+current+"'");
            
				while(result.next()){
					//System.out.println(result.getString("move"));
					
					size++;
					data.put(size,result.getString("move") );
					// returnThis=result.getString("move");
				}
            
        }catch(Exception e){
              System.out.println("DB DOWN");
        }
         
         
         n =rn.nextInt(size - 1 + 1) + 1;
         
         //Initialize variables
        
         //get a random sequence matching prefix from HashMap
         //System.out.println(n+"SIZE"+size);
         tempSeq=data.get(n);
         int min=current.length()-1;
         int max=tempSeq.length()-1;
         
         int randomNum = rand.nextInt(max - min + 1) + min;
         
         char tempChar = tempSeq.charAt(randomNum);
         tempSeq = String.valueOf(tempChar);
         return tempSeq;
    }
 
   public void storePatternX(String pattern,String patternFull){
       
        Connection myConn=null;
        Statement stmt=null;
        ResultSet result=null;
        String user="root";
        String pass="";
        
        
        try{
             
           myConn=DriverManager.getConnection("jdbc:mysql://localhost/learndots",user,pass);
           stmt=myConn.createStatement();
           String sql = "INSERT INTO `learndots`.`learnx` (`id`, `pathS`, `pathF`) VALUES (NULL,'"+pattern+"','"+patternFull+"')" ;
           stmt.executeUpdate(sql);
                       
        }catch(SQLException e){
              System.out.println(e.getMessage());
              //System.out.println(patternFull);
        }
       
       
       
       
    }
   
   public void storePatternY(String pattern,String patternFull){
       
        Connection myConn=null;
        Statement stmt=null;
        ResultSet result=null;
        String user="root";
        String pass="";
        
        
         try{
             
           myConn=DriverManager.getConnection("jdbc:mysql://localhost/learndots",user,pass);
           stmt=myConn.createStatement();
           String sql = "INSERT INTO `learndots`.`learny` (`id`, `pathS`, `pathF`) VALUES (NULL,'"+pattern+"','"+patternFull+"')" ;
           stmt.executeUpdate(sql);
                       
        }catch(Exception e){
              System.out.println("DB DOWN");
        }
       
       
       
       
       
     }
   
    public String returnBestVer1(String current){
        
        String tempSeq;
        int messageSize;
        String resultPlayer;
        Connection myConn=null;
        Statement stmt=null;
        ResultSet result=null;
        String user="root";
        String pass="";
        int size=0;
        int n=0;
		
        Random rand = new Random();
        HashMap <Integer,String> data= new HashMap <>();
        Random rn = new Random();
        String returnThis = "NOPE";
		
        try{
             
            n=0;
            size=0;
            data.clear();
            messageSize=0;
            tempSeq=null;
              
			myConn=DriverManager.getConnection("jdbc:mysql://localhost/learndots",user,pass);
			stmt=myConn.createStatement();
			result=stmt.executeQuery("SELECT * FROM `learny` WHERE move like '"+current+"'");
            
			while(result.next()){
				size++;
				data.put(size,result.getString("move") );
				
			}
            
        }catch(Exception e){
              System.out.println("DB DOWN");
        }
         
         
         n =rn.nextInt(size - 1 + 1) + 1;
         
         //Initialize variables
        
         //get a random sequence matching prefix from HashMap
         //System.out.println(n+"SIZE"+size);
         
		 tempSeq=data.get(n);
         int indexOfit = current.indexOf("%");               
         char tempChar = tempSeq.charAt(indexOfit);
         tempSeq = String.valueOf(tempChar);
		 
         return tempSeq;
     }
    
    public boolean isitWise(String current,String side){
        String db="";
        boolean wise=false;
        if(side.equals("x")){
            db="learnx";
        }else{
            db="learny";
        }
        
        String tempSeq;
        int messageSize;
        String resultPlayer;
        Connection myConn=null;
        Statement stmt=null;
        ResultSet result=null;
        String user="root";
        String pass="";
        int size=0;
        int n=0;
		
        Random rand = new Random();
        HashMap <Integer,String> data= new HashMap <>();
        Random rn = new Random();
        String returnThis = "NOPE";
		
        try{
             
                n=0;
                size=0;
                data.clear();                messageSize=0;
                tempSeq=null;
                
				myConn=DriverManager.getConnection("jdbc:mysql://localhost/learndots",user,pass);
				stmt=myConn.createStatement();
				result=stmt.executeQuery("SELECT * FROM '"+db+"' WHERE move like '"+current+"'");
            
				while(result.next()){
					size++;
					data.put(size,result.getString("move") );
					
				}
            
        }catch(Exception e){
              System.out.println("DB DOWN");
        }
    
         if(size>0){
             wise=true;
         }
        
        return wise;
    }
    
    public String getWinningSeq(String chosenByOpponent,String playerTemp,String player){
        String sym="%";
        String playerChosen=sym+playerTemp+sym;
        int maxComparisons = 0;
       
        
        String db=null;
        if(player.equals("X")){
            db="learnx";
        }else{
            db="learny";
        }
        
        boolean avoidDuplicates=true;
        String tempSeq = null;
        int messageSize;
        String resultPlayer;
        Connection myConn=null;
        Statement stmt=null;
        ResultSet result=null;
        String user="root";
        String pass="";
        int size=0;
        int n=0;
		
        
        Random rand = new Random();
        HashMap <Integer,String> data= new HashMap <>();
        Random rn = new Random();
        String returnThis = "NOPE";
        
        try{
             
        n=0;
        size=0;
        data.clear();                
		messageSize=0;
        tempSeq=null;
        
		myConn=DriverManager.getConnection("jdbc:mysql://localhost/learndots",user,pass);
		stmt=myConn.createStatement();
        
		result=stmt.executeQuery("SELECT * FROM '"+db+"' WHERE pathS like '"+playerChosen+"'");
         
		while(result.next()){
			size++;
			data.put(size,result.getString("pathS") );
			
		}
        
        while(avoidDuplicates){
            maxComparisons++;
            try{
            n =rn.nextInt(size - 1 + 1) + 1;    
            }catch(IllegalArgumentException catchMe){
                n=1; //To allow the program to be running, worse case scenario of an exception occurs.
            }
            tempSeq=data.get(n);

            
        try{
            int index=tempSeq.indexOf(playerTemp);
            
            if(index==1){
                index=0;
            }else if(index==0){
                index=tempSeq.length()-1;
            }else if(index==2){
                index=rn.nextInt(1 - 0 + 1) + 0;  
            }else
                index=index+1;
        
            
            int length=playerChosen.length();
            char tempChar = tempSeq.charAt(index);
            tempSeq = String.valueOf(tempChar);
         
        }catch(NullPointerException catchMe){
            System.out.println(catchMe.getMessage());
        }    
            
            avoidDuplicates = chosenByOpponent.contains(tempSeq);
            if(maxComparisons>300){
                char temp=playerTemp.charAt(0);
                tempSeq = String.valueOf(temp);
                avoidDuplicates=false;
            }
            
        }
           
           
           
        }catch(SQLException e){
        //      System.out.println(e.printStackTrace());
        }
        return tempSeq;
    }
    
    
    
 }
