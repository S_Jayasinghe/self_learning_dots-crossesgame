public class gameLogic {

    private char[][] board; 
    private char currentPlayerMark;
    private String resultPlayer;
    private int ROW;
    private int COL;
    private int draw; 
    private int isDraw;
    //true = x
    //false  = y
    private boolean rows;
    private boolean columns;
    private boolean diag;
    
    
    public gameLogic() {
        board = new char[3][3];
        currentPlayerMark = 'x';
        initializeBoard();
    }
	
	
    // Set/Reset the board back to all empty values.
    public void initializeBoard() {
        draw=0;
        isDraw=1;
        rows=false;
        columns=false;
        
        // Loop through rows
        for (int i = 0; i < 3; i++) {
			
            // Loop through columns
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
	
	
    // Print the current board (may be replaced by GUI implementation later)
    public void printBoard() {
        System.out.println("-------------");
		
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }
	
	
    // Loop through all cells of the board and if one is found to be empty (contains char '-') then return false.
    // Otherwise the board is full.
    public boolean isBoardFull() {
        boolean isFull = true;
		
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    isFull = false;
                }
            }
        }
		
        return isFull;
    }
	
    public String returnPlayer(){
        
        
        if(rows||columns||diag){
            resultPlayer="X";
       }else{
            resultPlayer="Y";
        }
        
        return resultPlayer;
    }
    
    // Returns true if there is a win, false otherwise.
    // This calls our other win check functions to check the entire board.
    public boolean  checkForWin() {
        draw=0;
        return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
    }
	
	
    // Loop through rows and see if any are winners.
    private boolean checkRowsForWin() { 
        for (int i = 0; i < 3; i++) {
            
            
            if (checkRowCol(board[i][0], board[i][1], board[i][2]) == true) {
                
                if((board[i][0]=='x')&&(board[i][1]=='x')&&(board[i][2]=='x')){
                    rows=true;     draw++;             
                }else if((board[i][0]=='y')&&(board[i][1]=='y')&&(board[i][2]=='y')){
                    rows=false;    draw++; 
                }
                //System.out.println("Draw:But the first player to finish is won! ");
                return true;
            }
        }
        return false;
    }
	
	
    // Loop through columns and see if any are winners.
    private boolean checkColumnsForWin() { 
        for (int i = 0; i < 3; i++) {
            if (checkRowCol(board[0][i], board[1][i], board[2][i]) == true) {
                if((board[0][i]=='x')&&(board[1][i]=='x')&&(board[2][i]=='x')){
                   columns=true; 
                }else if((board[0][i]=='y')&&(board[1][i]=='y')&&(board[2][i]=='y')){
                   columns=false; 
                }
                
              
                return true;
            }
        }
        return false;
    }
	
	
    // Check the two diagonals to see if either is a win. Return true if either wins.
    private boolean checkDiagonalsForWin() {
        if(((board[0][0]=='x')&&(board[1][1]=='x')&&(board[2][2]=='x')) ||
            (board[0][2]=='x')&&(board[1][1]=='x')&&(board[2][0]=='x')
                )
        {
           diag=true; 
        }else{
           diag=false;
        }
        
        return ((checkRowCol(board[0][0], board[1][1], board[2][2]) == true) || (checkRowCol(board[0][2], board[1][1], board[2][0]) == true));
    }
	
	
    //To check whether the occuring values are the same
    private boolean checkRowCol(char c1, char c2, char c3) {
        
        return ((c1 != '-') && (c1 == c2) && (c2 == c3));
    }
	
	
    // Change player marks back and forth.
    public void changePlayer(char type) {
        
//        if (currentPlayerMark == 'x') {
//            currentPlayerMark = 'o';
//        }
//        else {
//            currentPlayerMark = 'x';
//        }
           currentPlayerMark=type;
    }
	
	
    // Places a mark at the cell specified by row and col with the mark of the current player.
    public boolean placeMark(String posi) {
        int pos = Integer.parseInt(posi);
	int row = 0,col=0;
        
        switch (pos) {
            case 1:
                row=0;
                col=0;
                break;
            case 2:
                row=0;
                col=1;
                break;
            case 3:
                row=0;
                col=2;
                break;
            case 4:
                row=1;
                col=0;
                break;
            case 5:
                row=1;
                col=1;
                break;
            case 6:
                row=1;
                col=2;
                break;
            case 7:
                row=2;
                col=0;
                break;
            case 8:
                row=2;
                col=1;
                break;
            case 9:
                row=2;
                col=2;
                break;
            default:
                break;
        }
        
        
        
        
        // Make sure that row and column are in bounds of the board.
        if ((row >= 0) && (row < 3)) {
            if ((col >= 0) && (col < 3)) {
                if (board[row][col] == '-') {
                    board[row][col] = currentPlayerMark;
                    return true;
                }
            }
        }
		
        return false;
    }
    
    public int returnDraw(){
        return isDraw;
    }
}