
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Stream;
import java.sql.*;
import java.util.Random;
import java.util.Arrays;
import java.math.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author S.Jayasinghe
 */

    
public class starter {

    public static void main(String[]args) throws FileNotFoundException, IOException{
        
        gameLogic game = new gameLogic();
        
        String resultPlayer;

        int n=0;
        int [] A = new int[3];
        int [] B = new int[3];
        Random rand = new Random();
        String concatPath;
        String concatX=null;
        String concatY=null;
        String sym="%";
        int IN=0;
        String concat=null;
        boolean intelDecider=false;
        game.initializeBoard();
        
        //Query database and obtain values for the two arrays A and B
        random one= new random();
        
      for(int i=0;i<100;i++){
           intelDecider=getRandomBoolean();
           //intelDecider=true;
           
           if(!intelDecider){
           
                //System.out.println("#Playing game without using stored patterns#");
                game.initializeBoard();

                IN = rand.nextInt(9 - 1 + 1) + 1;

                String input=Integer.toString(IN);                   
                game.changePlayer('x');
                game.placeMark(input);  //A1
                concatX=input;
                

                concat=input;
                String temp=input+sym; 

               
                temp=one.returnPattern(temp); //B1
                game.changePlayer('y');
                game.placeMark(temp); 
                concatY=temp;
                //System.out.println("Y1=="+temp);
                concat=concat+temp;

                temp=concat+sym;
               // System.out.println(temp);
                temp=one.returnPattern(temp); //A2
                game.changePlayer('x');
                game.placeMark(temp); 
                concatX=concatX+temp;
                //System.out.println("X2=="+temp);
                concat=concat+temp;

                temp=concat+sym;
               // System.out.println(temp);  //B2
                temp=one.returnPattern(temp);
                game.changePlayer('y');
                game.placeMark(temp);
                concatY=concatY+temp;
                //System.out.println("Y2=="+temp);
                concat=concat+temp;

                temp=concat+sym;
               // System.out.println(temp);  //A3
                temp=one.returnPattern(temp); 
                game.changePlayer('x');
                game.placeMark(temp);
                concatX=concatX+temp;
                //System.out.println("X3=="+temp);
                concat=concat+temp;

                temp=concat+sym;
               // System.out.println(temp);  //B3
                temp=one.returnPattern(temp); 
                game.changePlayer('y');
                //System.out.println("Y3=="+temp);
                game.placeMark(temp);
                concatY=concatY+temp;
                concat=concat+temp;

                game.printBoard();
           
           }else if(intelDecider){
              System.out.println("#Playing game using stored patterns#"); 
       try{
               String temp=null;
               int x=0;
               int y=0;
               int randomStart[]=new int [2];
               
               
               //Select a random number for X and Y
               game.initializeBoard();
               randomStart[0]=rand.nextInt(9 - 1 + 1) + 1;
               randomStart[1]=rand.nextInt(9 - 1 + 1) + 1;
//               System.out.println("------------NEW-------------");
//               System.out.println("Random One "+randomStart[0]);
//               System.out.println("Random Two "+randomStart[1]);
               
               
               String input=Integer.toString(randomStart[0]);  
               //X1
               game.changePlayer('x');
               game.placeMark(input);  
               concatX=input;
//               System.out.println("------------------------------");
//               System.out.println("X1: "+input);
               x=0;
               concat=input;
               
               
               //Y1
               input=Integer.toString(randomStart[1]);  
//               System.out.println("Y1: "+input);
               game.changePlayer('y');
               
               game.placeMark(input); 
               concatY=input;
               concat=concat+input;
               
               
               //X2
//               System.out.println("ConcatY: "+concatY);
//               System.out.println("ConcatX: "+concatX);
               temp=one.getWinningSeq(concatY,concatX,"X"); 
               game.changePlayer('x');
               game.placeMark(temp); 
               concatX=concatX+temp;
//               System.out.println("X2:  "+temp);
                //System.out.println("X2=="+temp);
               concat=concat+temp;
               
               //Y2
               temp=one.getWinningSeq(concatX,concatY,"Y");
               game.changePlayer('y');
               game.placeMark(temp);
               concatY=concatY+temp;
//               System.out.println("Y2:  "+temp);
               concat=concat+temp;
               
               
               //X3
               temp=one.getWinningSeq(concatY,concatX,"X"); 
               game.changePlayer('x');
               game.placeMark(temp);
               concatX=concatX+temp;
//               System.out.println("X3:  "+temp);
               concat=concat+temp;
                
                //Y3
                temp=one.getWinningSeq(concatX,concatY,"Y");
                game.changePlayer('y');
//                System.out.println("Y3:  "+temp);
                game.placeMark(temp);
                concatY=concatY+temp;
                concat=concat+temp;

                game.printBoard();
                
                
       }catch(Exception handle){
           System.out.println("System Error Occured,Exception handled to avoid termination");
           continue;
       }
           
     }
        
       
        // Do we have a winner?
        if (game.checkForWin()) {
           resultPlayer=game.returnPlayer();
           System.out.println("Winning Game! Player is "+resultPlayer);
           if(resultPlayer.equals("X")){
               System.out.println("Path of Winner:"+concatX);
               
               //one.storePatternX(concatX, concat);
           }else if(resultPlayer.equals("X"))
           {
               System.out.println("Path of Winner:"+concatY );
               //System.out.println(game.returnDraw());
               //one.storePatternY(concatY, concat);
           }
           
           System.out.println("Complete Path :"+concat);
           //System.out.println(game.returnDraw());
           System.out.println();
        }
        
        else if (game.isBoardFull()) {
           System.out.println("Draw!");
        }
        else{
            System.out.println("No Winner!");
        }
            game.initializeBoard();
            
     } 
  
   }
    
    public static boolean getRandomBoolean() {
       return Math.random() < 0.5;
       //Returns a random True Or False! Assisting computer to choose being wise or not! 
   }
            
 
        
}

